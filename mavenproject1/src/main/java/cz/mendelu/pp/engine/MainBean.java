/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.engine;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import cz.mendelu.pp.database.FileManager;
import cz.mendelu.pp.database.RelationalDatabase;
import cz.mendelu.pp.models.FileRecord;
import cz.mendelu.pp.models.User;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jdk.nashorn.internal.objects.annotations.Property;
import org.apache.commons.io.FilenameUtils;
import org.bson.Document;

/**
 *
 * @author User_Petan
 */
@ManagedBean(name = "mainbean")
@ViewScoped
public class MainBean implements Serializable {

    private FileManager myFileManager = new FileManager();
    private String selectedExtension = "all";
    private String selectedCategory = "all";
    private Map<String, Integer> options = new HashMap<>();
    private String newCategory = null;
    private List<String> newCategories = new ArrayList();

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
        String q1 = bundle.getString("question1");
        String q2 = bundle.getString("question2");
        String q3 = bundle.getString("question3");
        String q4 = bundle.getString("question4");
        
        options.put(q1, 1);
        options.put(q2, 2);
        options.put(q3, 3);
        options.put(q4, 4);
        
    }
    
    
    public Map<String, Integer> getOptions() {
        return options;
    }

    public void setOptions(Map<String, Integer> options) {
        this.options = options;
    }



    public String getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public String getNewCategory() {
        return newCategory;
    }

    public void setNewCategory(String newCategory) {
        this.newCategory = newCategory;
    }

    public String getSelectedExtension() {
        return selectedExtension;
    }

    public void addToNewCategories() {
        newCategories.add(newCategory);
        System.out.println("nova kategorie" + newCategory);
        System.out.println("vypis novych pocet: " + newCategories.size());
    }

    public void setSelectedExtension(String selectedExtension) {
        this.selectedExtension = selectedExtension;
    }

    // Vrátí všechny přípony ,které se nacházejí u uživatelových souborů
    public List<String> getExtensions() throws ParseException {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        User u = (User) session.getAttribute("user");

        Map<String, String> filters = new HashMap();
        filters.put("username", u.getId_user());
        List<String> resultExtensions = new ArrayList();

        for (FileRecord fr : myFileManager.find(filters)) {
            if (!resultExtensions.contains(fr.extension)) {
                resultExtensions.add(fr.extension);
            }
        }
        return resultExtensions;
    }

    // Vrátí všechny přípony ,které se nacházejí u uživatelových souborů
    public List<String> getAllCategories() throws ParseException {
        Map<String, String> filters = new HashMap();
        List<String> resultCategories = new ArrayList();

        for (FileRecord fr : myFileManager.find(filters)) {
            if (fr.getCategories() != null) {
                List<String> fr_categories = fr.getCategories();
                for (String fr_category : fr_categories) {
                    if (!resultCategories.contains(fr_category)) {
                        resultCategories.add(fr_category);
                    }
                }
            }
        }

        if (newCategories.size() > 0) {
            for (String newCategory : newCategories) {
                if (!resultCategories.contains(newCategory) && newCategory.length() > 0) {
                    resultCategories.add(newCategory);
                }
            }
        }

        return resultCategories;
    }

    // Vrátí všechny přípony ,které se nacházejí u uživatelových souborů
    public List<String> getMyCategories() throws ParseException {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        User u = (User) session.getAttribute("user");

        Map<String, String> filters = new HashMap();
        filters.put("username", u.getId_user().toString());
        List<String> resultCategories = new ArrayList();

        for (FileRecord fr : myFileManager.find(filters)) {
            if (fr.getCategories() != null) {
                List<String> fr_categories = fr.getCategories();
                for (String fr_category : fr_categories) {
                    if (!resultCategories.contains(fr_category)) {
                        resultCategories.add(fr_category);
                    }
                }
            }
        }

        return resultCategories;
    }

    // vrátí seznam všech souborů ,které uživatel vlastní
    public List<FileRecord> getMyBrief() throws ParseException {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        User u = (User) session.getAttribute("user");

        Map<String, String> filters = new HashMap();
        filters.put("username", u.getId_user().toString());

        if (!selectedExtension.trim().equalsIgnoreCase("all")) {
            filters.put("extension", selectedExtension);
        }

        if (!selectedCategory.trim().equalsIgnoreCase("all")) {
            filters.put("categories", selectedCategory);
        }

        return myFileManager.find(filters);
    }

    public void callFileDelete(Integer id, String fileUrl) {

        // nejprve vymazu vyzicky soubor
        File file = new File(fileUrl);

        if (file.delete()) {
            System.out.println(file.getName() + " is deleted!");
        } else {
            System.out.println("Delete operation is failed.");
        }

        // nasledne vymazu zaznam z mongoDB
        myFileManager.delete(id);

        // TODO
        // tady by to bylo úžasný nějak vypsat hlášku o provedeným vymazáním
    }

    public String getUserName() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        User u = (User) session.getAttribute("user");
        return u.getName();
    }

    public String getUserId() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        User u = (User) session.getAttribute("user");
        return u.getId_user().toString();
    }

    public String getUserEmail() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        User u = (User) session.getAttribute("user");
        return u.getEmail();
    }

    public Boolean isRemoteLogged() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        User u = (User) session.getAttribute("user");

        if (u.getLoginType().equals("relationalDb")) {
            return false;
        } else {
            return true;
        }
    }

    public String getUserProfilePicture() throws UnirestException, ClassNotFoundException, SQLException, IOException {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        User u = (User) session.getAttribute("user");

        // defaultne tam bude jen panacek..
        String image_url = "http://d22r54gnmuhwmk.cloudfront.net/app-assets/global/default_user_icon-7a95ec473c1c41f6d020d32c0504a0ef.jpg";

        // jen pokud je typ loginu vzdálené tak se bude tahat profilovka od nekud
        if (u.getLoginType().equals("facebook")) {
            HttpResponse<String> response = Unirest.get("http://graph.facebook.com/" + u.getId_user() + "/picture?type=large&redirect=false").asString();
            JsonParser parser = new JsonParser();
            JsonObject o = parser.parse(response.getBody()).getAsJsonObject();
            image_url = o.get("data").getAsJsonObject().get("url").getAsString();
        }

        if (u.getLoginType().equals("google")) {
            image_url = u.getPicture_url();
        }

        if (u.getLoginType().equals("relationalDb")) {
            RelationalDatabase db = new RelationalDatabase();
            byte[] imageByteArray = db.getUserPicture(u.getId_user());
            image_url = javax.xml.bind.DatatypeConverter.printBase64Binary(imageByteArray);
            System.out.println("IMAGE URL VRACEN ? " + image_url.length());
        }

        return image_url;
    }

    //logout event, invalidate session
    public void logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.invalidate();

        // redirect na index
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest origRequest = (HttpServletRequest) context.getExternalContext().getRequest();
        String contextPath = origRequest.getContextPath();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("../login.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void downloadFile(String filepath, String filename) {
        System.out.println("jsem v download");
        System.out.println("filepath" + filepath);
        System.out.println("filename" + filename);

        File file = new File(filepath);
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        response.setHeader("Content-Disposition", "attachment;filename=" + filename);
        response.setContentLength((int) file.length());
        ServletOutputStream out = null;
        try {
            FileInputStream input = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            out = response.getOutputStream();
            int i = 0;
            while ((i = input.read(buffer)) != -1) {
                out.write(buffer);
                out.flush();
            }
            FacesContext.getCurrentInstance().getResponseComplete();
        } catch (IOException err) {
            err.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException err) {
                err.printStackTrace();
            }
        }

    }

}
