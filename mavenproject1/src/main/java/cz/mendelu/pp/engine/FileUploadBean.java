/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.engine;

import cz.mendelu.pp.database.FileManager;
import cz.mendelu.pp.models.FileRecord;
import cz.mendelu.pp.models.User;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author User_Petan
 */
@ManagedBean
@ViewScoped
public class FileUploadBean {

    private FileManager myFileManager = new FileManager();
    private UploadedFile file = null;

    private List<String> selectedCategories = new ArrayList<>();

    private Integer uploadDays = 5;

    private final String filesFolder = "C:\\data";

    public UploadedFile getFile() {
        return file;
    }

    public Integer getUploadDays() {
        return uploadDays;
    }

    public void setUploadDays(Integer uploadDays) {
        this.uploadDays = uploadDays;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public List<String> getSelectedCategories() {
        return selectedCategories;
    }

    public void setSelectedCategories(List<String> selectedCategories) {
        this.selectedCategories = selectedCategories;
    }

    public String getUploadStatus() {
        if (file == null) {
            return  "Vyberte prosím soubor.";
        } else {
            return file.getFileName() + " - nahrán na server.";
        }
    }

    public void upload() {
        if (file.getFileName().length() > 1) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            FacesMessage message = new FacesMessage("Upload error.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

    }

    public void upload2(FileUploadEvent event) {

    }

    public void confirm() throws Exception {

        // jestliže byl nahrán soubor
        if (file != null && file.getFileName().length() >= 1) {
            
            FacesMessage message = new FacesMessage("Ano, zapsani souboru, zavolani db a redirect");
            FacesContext.getCurrentInstance().addMessage(null, message);

            // Fyzické vytvoření souboru
            String filename = FilenameUtils.getName(file.getFileName());
            InputStream input = file.getInputstream();

            OutputStream output = new FileOutputStream(new File(filesFolder, filename));
            IOUtils.copy(input, output);
            String filePath = filesFolder + "\\" + filename;
            // Vytvoreni FileRecordu
            createFileRecord(filePath);
            output.close();
            input.close();
            
            System.out.println("jjo nahrani bylo dobre");

        } else {
            if(file == null)System.out.println("file je null");
            // jestliže nebyl nahranej žadnej soubor
            FacesMessage message = new FacesMessage("Nebyl nahrán žádný soubor!");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void createFileRecord(String filePath) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        User u = (User) session.getAttribute("user");
        File f = new File(filePath);
        LocalDate now = LocalDate.now();

        String sizeString = String.format("%.2f", (double) f.length() / 1000000);

        // vytvoreni objektu zaznamu souboru
        FileRecord newFileRecord = new FileRecord(
                f.getName(),
                u.getId_user(),
                FilenameUtils.getExtension(filePath.toLowerCase()),
                sizeString,
                Date.from(now.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()),
                Date.from(now.plusDays(uploadDays).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()),
                filePath
        );

        // jestliže byly vybrany nějaké kategorie
        if (!selectedCategories.isEmpty()) {
            newFileRecord.setCategories(selectedCategories);
        }

        try {
            // fyzicke vloženi do db
            myFileManager.insert(newFileRecord);

            // redirect
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest origRequest = (HttpServletRequest) context.getExternalContext().getRequest();
            String contextPath = origRequest.getContextPath();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (ParseException ex) {
            FacesMessage message = new FacesMessage("Problém při nahrávání do NOSQL databáze");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

    }

}
