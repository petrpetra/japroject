/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.login;

import cz.mendelu.pp.database.RelationalDatabase;
import cz.mendelu.pp.models.User;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

@ManagedBean
@SessionScoped
public class LoginResolver implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;
    private User user;

    public void validate() throws ClassNotFoundException, SQLException {

        RelationalDatabase db = new RelationalDatabase();

        HttpSession session = (HttpSession) FacesContext.
                getCurrentInstance().getExternalContext().getSession(true);
        user = (User) session.getAttribute("user");
        user.setLoginType("relationalDb");

        boolean valid = db.verify(user.getName(), user.getPassword());

        // jestli login probehl uspesne tak nastavim session a redirect
        if (valid) {
            session.setAttribute("user", user);
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest origRequest = (HttpServletRequest) context.getExternalContext().getRequest();
            String contextPath = origRequest.getContextPath();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("secured/index.xhtml");
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
            String wrong_login = bundle.getString("wrong_login");
            String login = bundle.getString("login");
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,login , wrong_login);
            RequestContext.getCurrentInstance().showMessageInDialog(message);

        }

    }

}
