/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.login;

import cz.mendelu.pp.database.RelationalDatabase;
import cz.mendelu.pp.engine.MainBean;
import cz.mendelu.pp.helpers.PasswordHelper;
import cz.mendelu.pp.models.User;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
//import org.apache.commons.collections4.MapUtils;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Petra
 */
@ManagedBean
@SessionScoped
public class ResetPasswordResolver implements Serializable {

    private User user;
    RelationalDatabase db;

    private Boolean step = false;

    public Boolean getStep() {
        return step;
    }

    public ResetPasswordResolver() throws ClassNotFoundException, SQLException {
        this.db = new RelationalDatabase();
        HttpSession session = (HttpSession) FacesContext.
                getCurrentInstance().getExternalContext().getSession(true);
        user = (User) session.getAttribute("user");
        user.setLoginType("relationalDb");

    }

    public String verifyIdentity() throws ClassNotFoundException, SQLException {

        System.out.println("Zacatelk verify" + user.toString());

        boolean valid = db.verifyIdentity(user.getName(), user.getEmail());

        //Map<Integer, String> reversedHashMap = MapUtils.invertMap(db.getOptions());

        Map<Integer, String> reversedHashMap= null;
        if (valid) {
            user.setVerificationQuestion(reversedHashMap.get(Integer.valueOf(user.getVerificationQuestion())));

            step = true;
            System.out.println("otazka ted " + user.getVerificationQuestion());

        } else {
            FacesMessage message = new FacesMessage("Neexistuje taková kombinace jména a mailové adresy.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

        System.out.println("Konec verify" + user.toString());

        return "resetpassword.xhtml";

    }

    public void resetPassword() throws ClassNotFoundException, SQLException {
        if (db.verifyAnswer(user.getId_user(), PasswordHelper.generateHash(user.getVerificationAnswer()))) {
            System.out.println("chci udělat reset");
            System.out.println(user.toString());

            String hash = PasswordHelper.generateHash(user.getPassword());
            Integer i = db.updatePassword(Integer.parseInt(user.getId_user()), hash);
            if (i > 0) {
                System.out.println("UPDATNTOOOOOOOOOOOOOOOOOOOOO");
                HttpSession session = (HttpSession) FacesContext.
                        getCurrentInstance().getExternalContext().getSession(true);
                FacesContext context = FacesContext.getCurrentInstance();
                ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
                String title = bundle.getString("successful_signup_title");
                String msg_susccessful = bundle.getString("content");
                String continue_msg = bundle.getString("continue");
                session.invalidate();
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, title, msg_susccessful + "<a href='login.xhtml'>" + ". " + "<h3 align='center'>" + continue_msg + "</h3>" + "</a>"));
            }

        } else {
            FacesMessage message = new FacesMessage("Špatná odpověď na kontrolni otázku. Zkuste to prosím znovu.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

    }
}
