package cz.mendelu.pp.login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cz.mendelu.pp.models.User;
import javax.servlet.http.HttpSession;

public class GoogleLogin extends HttpServlet {

    public GoogleLogin() {
        super();

    }

    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        try {
            String code = request.getParameter("code");
            String urlParameters = "code="
                    + code
                    + "&client_id=160172253946-a1fa1oktmssaaet2p4ktfnog0abh3fb0.apps.googleusercontent.com"
                    + "&client_secret=qVWO4_Imb1CM8SK8INek2PYx"
                    + "&redirect_uri=http://localhost:8080/japroject/sev"
                    + "&grant_type=authorization_code";

            URL url = new URL("https://accounts.google.com/o/oauth2/token");

            URLConnection urlConn = url.openConnection();
            urlConn.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(urlConn.getOutputStream());
            writer.write(urlParameters);
            writer.flush();

            String line, outputString = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }

            JsonObject json = (JsonObject) new JsonParser().parse(outputString);
            String access_token = json.get("access_token").getAsString();
            System.out.println(access_token);

            url = new URL("https://www.googleapis.com/oauth2/v1/userinfo?access_token="+ access_token);
            urlConn = url.openConnection();
            outputString = "";
            reader = new BufferedReader(new InputStreamReader(
                    urlConn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }
            System.out.println(outputString);

            JsonParser parser = new JsonParser();
            JsonObject o = parser.parse(outputString).getAsJsonObject();

            User u = new User(o.get("id").getAsString(), o.get("name").getAsString(), "google");
            u.setPicture_url(o.get("picture").getAsString());
            
            System.out.print(o.get("picture").getAsString());

            HttpSession httpSession = request.getSession();

            httpSession.setAttribute("user", u);

            // presmeruji ted jiz prihlaseneho uzivatele na jeho stranku
            response.sendRedirect("secured/index.xhtml");

        } catch (MalformedURLException e) {
            System.out.println(e);
        } catch (ProtocolException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }

        System.out.println("leaving doGet");

    }

}
