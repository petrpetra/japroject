/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.login;

import com.github.scribejava.apis.FacebookApi;
import com.github.scribejava.apis.GoogleApi;
import com.github.scribejava.apis.LinkedInApi;
import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import static com.github.scribejava.core.model.OAuthConstants.EMPTY_TOKEN;
import com.github.scribejava.core.oauth.OAuthService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cz.mendelu.pp.models.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Token;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.model.Verifier;

/*import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;*/

/**
 *
 * @author User_Petan
 */
public class RemoteLoginResolver extends HttpServlet {

    // tento servlet se bude volat jenom metodou GET
    // zde se rozhodne na jakou službu se přihlašuje
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("site").equals("google")) {
            googleProcess(request, response);
        }

        // jestliže parametr v URL "site" = facebook ...
        if (request.getParameter("site").equals("facebook")) {
            facebookProcess(request, response);
        }

        // jestliže parametr v URL "site" = linkedin ...
        if (request.getParameter("site").equals("linkedin")) {
            linkedinProcess(request, response);
        }

        // jestliže parametr v URL "site" = linkedin ...
        if (request.getParameter("site").equals("twitter")) {
            twitterProcess(request, response);

        }

    }

    public void facebookProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // fb app udaje
        String api_key = "447567858764225";
        String app_sacret = "4410bd7b8e375e14cfcd3705fc89a9e5";

        // jestli se nam jeste nevraci facebook kod ,tak se bude spojeni teprve inicializovat
        if (request.getParameter("code") == null) {
            // vyberu session
            HttpSession session = request.getSession();

            // sluzba pro facebook token
            OAuthService service = new ServiceBuilder()
                    .provider(FacebookApi.class)
                    .apiKey(api_key)
                    .apiSecret(app_sacret)
                    .callback(request.getRequestURL() + "?site=facebook")
                    .build();

            // tuhle sluzbu jeste nahraju do session
            session.setAttribute("serviceObject", service);

            // sluzba vytvori autorizacni url
            Token token = new Token(api_key, app_sacret);
            final String authorizationUrl = service.getAuthorizationUrl(token);
            System.out.println("url fb " + authorizationUrl);
            // na url uzivatele presmeruji
            response.sendRedirect(authorizationUrl);
        }

        // jestli se nam uz vraci facebook kod ,tak se uz budeme ptat na uzivatele
        if (request.getParameter("code") != null) {
            // vyberu session
            HttpSession session = request.getSession();

            // v session jsem si ulozis ten service objekt
            OAuthService service = (OAuthService) session.getAttribute("serviceObject");

            // scribejava black magic d|O_O|b
            final Verifier verifier = new Verifier(request.getParameter("code"));
            final Token accessToken = service.getAccessToken(EMPTY_TOKEN, verifier);
            //final Token accessToken = service.getAccessToken("447567858764225|hzwPKqGCPWIrzRoHV5pMNDi1d1E", verifier);

            final OAuthRequest rrequest = new OAuthRequest(Verb.GET, "https://graph.facebook.com/v2.2/me", service);
            //final OAuthRequest rrequest = new OAuthRequest(Verb.GET, "https://docs.google.com/feeds/default/private/full/", service);

            service.signRequest(accessToken, rrequest);
            final Response fb_response = rrequest.send();

            // odpoved fb_response.getBody() je v JSON ,takže parsu "name" a "id"
            JsonParser parser = new JsonParser();
            JsonObject o = parser.parse(fb_response.getBody()).getAsJsonObject();

            System.out.println(fb_response.toString());

            // a nastavim tyhle hodnoty do session ,aby si je pak prebral MainBean
            HttpSession httpSession = request.getSession();

            User u = new User(o.get("id").getAsString(), o.get("name").getAsString(), "facebook");

            httpSession.setAttribute("user", u);

            // presmeruji ted jiz prihlaseneho uzivatele na jeho stranku
            response.sendRedirect("secured/index.xhtml");

        }

    }

    public void googleProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("https://accounts.google.com/o/oauth2/auth?scope=email&redirect_uri=http://localhost:8080/japroject/sev&response_type=code&client_id=160172253946-a1fa1oktmssaaet2p4ktfnog0abh3fb0.apps.googleusercontent.com&%20approval_prompt=force");
    }

    public void twitterProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /*
        String testStatus = "Hello from twitter4j";

        PrintWriter out = response.getWriter();

        ConfigurationBuilder cb = new ConfigurationBuilder();

        //the following is set without accesstoken- desktop client
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("hFzeuZTOgZyrKkCuZ38GKmlKI")
                .setOAuthConsumerSecret("8faDcfHoQArZC4rY8ICdShm5T5ZIi4ZreDmdQycNHuXlk5cOhj");

        try {
            TwitterFactory tf = new TwitterFactory(cb.build());
            Twitter twitter = tf.getInstance();

            try {
                System.out.println("-----");

                // get request token.
                // this will throw IllegalStateException if access token is already available
                // this is oob, desktop client version
                RequestToken requestToken = twitter.getOAuthRequestToken();

                System.out.println("Got request token.");
                System.out.println("Request token: " + requestToken.getToken());
                System.out.println("Request token secret: " + requestToken.getTokenSecret());

                System.out.println("|-----");

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Open the following URL and grant access to your account:");
                response.sendRedirect(requestToken.getAuthorizationURL());
                System.out.println(requestToken.getAuthorizationURL());
                System.out.print("Enter the PIN(if available) and hit enter after you granted access.[PIN]:");

                out.println("AHOJ");
                
                out.close();

                String pin = br.readLine();

                AccessToken accessToken = null;
                accessToken = twitter.getOAuthAccessToken(requestToken);

                try {
                    if (pin.length() > 0) {
                        accessToken = twitter.getOAuthAccessToken(requestToken, pin);
                    } else {
                        accessToken = twitter.getOAuthAccessToken(requestToken);
                    }
                } catch (TwitterException te) {
                    if (401 == te.getStatusCode()) {
                        System.out.println("Unable to get the access token.");
                    } else {
                        te.printStackTrace();
                    }
                }

                System.out.println("Got access token.");
                System.out.println("Access token: " + accessToken.getToken());
                System.out.println("Access token secret: " + accessToken.getTokenSecret());

            } catch (IllegalStateException ie) {
                // access token is already available, or consumer key/secret is not set.
                if (!twitter.getAuthorization().isEnabled()) {
                    System.out.println("OAuth consumer key/secret is not set.");
                    System.exit(-1);
                }
            }

            Status status = twitter.updateStatus(testStatus);

            System.out.println("Successfully updated the status to [" + status.getText() + "].");

            System.out.println("ready exit");

            System.exit(0);
        } catch (Exception te) {
            te.printStackTrace();
            System.out.println("Failed to get timeline: " + te.getMessage());
            System.exit(-1);
        }
         */
    }

    public void linkedinProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // fb app udaje
        String api_key = "77zr3yq7pjakl1";
        String app_sacret = "0oT8P4FHFVbq4i00";

        // jestli se nam jeste nevraci facebook kod ,tak se bude spojeni teprve inicializovat
        if (request.getParameter("code") == null) {
            // vyberu session
            HttpSession session = request.getSession();

            // sluzba pro facebook token
            OAuthService service = new ServiceBuilder()
                    .provider(LinkedInApi.class)
                    .apiKey(api_key)
                    .apiSecret(app_sacret)
                    .callback(request.getRequestURL() + "?site=linkedin")
                    .build();

            // tuhle sluzbu jeste nahraju do session
            session.setAttribute("serviceObject", service);

            // sluzba vytvori autorizacni url
            Token token = new Token(api_key, app_sacret);
            final String authorizationUrl = service.getAuthorizationUrl(token);
            System.out.println("url z linked " + authorizationUrl);
            // na url uzivatele presmeruji
            response.sendRedirect(authorizationUrl);
        }

        // jestli se nam uz vraci facebook kod ,tak se uz budeme ptat na uzivatele
        if (request.getParameter("code") != null) {
            // vyberu session
            HttpSession session = request.getSession();

            // v session jsem si ulozis ten service objekt
            OAuthService service = (OAuthService) session.getAttribute("serviceObject");

            // scribejava black magic d|O_O|b
            final Verifier verifier = new Verifier(request.getParameter("code"));
            final Token accessToken = service.getAccessToken(EMPTY_TOKEN, verifier);
            //final Token accessToken = service.getAccessToken("447567858764225|hzwPKqGCPWIrzRoHV5pMNDi1d1E", verifier);

            final OAuthRequest rrequest = new OAuthRequest(Verb.GET, "http://api.linkedin.com/v1/people/~/connections:(id,last-name)", service);
            //final OAuthRequest rrequest = new OAuthRequest(Verb.GET, "https://docs.google.com/feeds/default/private/full/", service);

            service.signRequest(accessToken, rrequest);
            final Response fb_response = rrequest.send();

            // odpoved fb_response.getBody() je v JSON ,takže parsu "name" a "id"
            JsonParser parser = new JsonParser();
            JsonObject o = parser.parse(fb_response.getBody()).getAsJsonObject();

            System.out.println(fb_response.toString());

            response.sendRedirect("secured/index.xhtml");

        }

    }

}
