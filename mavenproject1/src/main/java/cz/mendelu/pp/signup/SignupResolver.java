/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.signup;

import cz.mendelu.pp.database.RelationalDatabase;
import cz.mendelu.pp.helpers.PasswordHelper;
import cz.mendelu.pp.models.User;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.Timer;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Petra
 */
@ManagedBean
@SessionScoped
public class SignupResolver implements Serializable {

    private User user;
    public UploadedFile file = null;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void checkImageUpload() {
        if (file.getFileName().length() > 1) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            FacesMessage message = new FacesMessage("Upload error.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void validate() throws ClassNotFoundException, SQLException, InvalidKeySpecException, NoSuchAlgorithmException, IOException {
        HttpSession session = (HttpSession) FacesContext.
                getCurrentInstance().getExternalContext().getSession(true);
        user = (User) session.getAttribute("user");
        RelationalDatabase db = new RelationalDatabase();
        int i = 0;


        if (!db.checkIfNameExists(user.getName())) {
            String hash = PasswordHelper.generateHash(user.getPassword());
            String answer_hash = PasswordHelper.generateHash(user.getVerificationAnswer());
            
            if(file == null){
                System.out.println("FILE JE NULL :(");
            }
            i = db.insertNewUser(user.getName(), user.getEmail(), hash, file, Integer.valueOf(user.getVerificationQuestion()),answer_hash);
        }
        if (i > 0) {
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
            String title = bundle.getString("successful_signup_title");
            String msg_susccessful = bundle.getString("content");
            String continue_msg = bundle.getString("continue");
            
            session.invalidate();
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, title, msg_susccessful + "<a href='login.xhtml'>"+ ". " + "<h3 align='center'>" + continue_msg + "</h3>"+ "</a>"));
            
            

        }

    }
}
