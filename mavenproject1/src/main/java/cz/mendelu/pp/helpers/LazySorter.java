/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.helpers;

/**
 *
 * @author User_Petan
 */
import cz.mendelu.pp.models.FileRecord;
import java.util.Comparator;
import org.primefaces.model.SortOrder;
 
public class LazySorter implements Comparator<FileRecord> {
 
    private String sortField;
     
    private SortOrder sortOrder;
     
    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(FileRecord file1, FileRecord file2) {
        try {
            Object value1 = FileRecord.class.getField(this.sortField).get(file1);
            Object value2 = FileRecord.class.getField(this.sortField).get(file2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}