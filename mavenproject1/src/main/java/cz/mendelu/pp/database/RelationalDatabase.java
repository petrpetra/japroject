/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.database;

import cz.mendelu.pp.helpers.PasswordHelper;
import cz.mendelu.pp.models.User;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author User_Petan
 */
@ManagedBean
@SessionScoped
public class RelationalDatabase implements Serializable {

    private Connection conn;
    private PreparedStatement ps = null;
    private User user;
    private Map<String, Integer> options = new HashMap<>();

    public RelationalDatabase() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        this.conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/java_projekt", "java_login", "java_login");
        HttpSession session = (HttpSession) FacesContext.
                getCurrentInstance().getExternalContext().getSession(true);
        user = (User) session.getAttribute("user");

        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
        String q1 = bundle.getString("question1");
        String q2 = bundle.getString("question2");
        String q3 = bundle.getString("question3");
        String q4 = bundle.getString("question4");

        options.put(q1, 1);
        options.put(q2, 2);
        options.put(q3, 3);
        options.put(q4, 4);
    }

    public Map<String, Integer> getOptions() {
        return options;
    }

    private void connectAgain() throws ClassNotFoundException, SQLException {
        if (this.conn.isClosed()) {
            Class.forName("org.postgresql.Driver");
            this.conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/java_projekt", "java_login", "java_login");
        }
    }

//vrati nulu pokud se dotaz nepovede
    public int insertNewUser(String name, String email, String password, UploadedFile image, int question, String answer) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException, IOException {

        byte[] content = null;
        if (image != null) {
            content = IOUtils.toByteArray(image.getInputstream());
        }

        int succesfullInsert = 0;
        try {
            String sql = "INSERT INTO USERS (USERNAME,EMAIL,PASSWORD,IMAGE, QUESTION, ANSWER)"
                    + "VALUES (?,?,?,?,?,?);";

            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, email);
            ps.setString(3, password);
            ps.setBytes(4, content);
            ps.setInt(5, question);
            ps.setString(6, answer);

            succesfullInsert = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return succesfullInsert;

    }

    public int updatePassword(Integer idUser, String password) throws ClassNotFoundException, SQLException {
        connectAgain();
        Integer successfulUpdate = 0;
        PreparedStatement stmt = null;
        String query = "UPDATE users SET password=? WHERE id_user=?";
        try {
            stmt = conn.prepareStatement(query);
            stmt.setString(1, password);
            stmt.setLong(2, idUser);
            successfulUpdate = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }

        return successfulUpdate;
    }

    public boolean verifyIdentity(String userName, String email) throws ClassNotFoundException, SQLException {
        connectAgain();
        boolean userExists = false;
        int question = 0;
        Integer id_user = 0;
        PreparedStatement stmt = null;
        String query = "SELECT * FROM users WHERE username =? AND email=?";
        try {
            stmt = conn.prepareStatement(query);
            stmt.setString(1, userName);
            stmt.setString(2, email);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                question = rs.getInt("question");
                id_user = rs.getInt("id_user");
                user.setVerificationQuestion(Integer.toString(question));
                user.setId_user(id_user.toString());

                System.out.println("db questino " + question);

            }
        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (question > 0) {
            userExists = true;
        }
        return userExists;

    }

    public Boolean verifyAnswer(String userId, String answeredQ) throws ClassNotFoundException, SQLException {
        connectAgain();
        boolean answerValid = false;
        String answer = "";
        Integer id_user = 0;
        PreparedStatement stmt = null;
        String query = "SELECT * FROM users WHERE id_user=?";
        try {
            stmt = conn.prepareStatement(query);
            stmt.setLong(1, Integer.valueOf(userId));
             ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                
                System.out.println("Porovnavan zadany hash : " + answeredQ);
                
                answer = rs.getString("answer");
                
                System.out.println("S tim co jsem našel v db k id : " + userId + " a to je: " + answeredQ);
                
                
                if (answeredQ.trim().equalsIgnoreCase(answer.trim())){
                    answerValid = true;
                    System.out.println("stejné!");
                }else {
                    System.out.println("jiné");   
                }
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }

        return answerValid;
    }

    // login verify
    public boolean verify(String userName, String pass) {

        String storedUsername = "";
        String storedPassword = "";
        boolean isAuthenticated = false;
        PreparedStatement stmt = null;
        String query = "SELECT * FROM users WHERE username =?";
        try {
            stmt = conn.prepareStatement(query);
            stmt.setString(1, userName);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                user.setId_user(rs.getString("id_user"));
                storedUsername = rs.getString("username");
                storedPassword = rs.getString("password");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(RelationalDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }

        String hashedPassword = PasswordHelper.generateHash(pass);
        if (hashedPassword.equals(storedPassword) && !"".equals(storedUsername)) {
            isAuthenticated = true;
        }

        return isAuthenticated;
    }

    public boolean checkIfNameExists(String name) throws SQLException {
        String sql = "SELECT * FROM users WHERE username = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, name);

        int rsCount = 0;
        ResultSet result = ps.executeQuery();
        while (result.next()) {
            rsCount++;
        }

        if (rsCount > 0) {
            return true;
        }
        return false;
    }

    public byte[] getUserPicture(String userId) throws SQLException {
        byte[] imageBytes = null;

        String sql = "SELECT image FROM users WHERE id_user = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, Integer.valueOf(userId));

        int rsCount = 0;
        ResultSet result = ps.executeQuery();

        System.out.println("Jsem po vykonani dotazu");

        while (result.next()) {
            System.out.println("jdu vybrat vysledek");
            imageBytes = result.getBytes("image");

        }

        System.out.println("vracim image velikost " + imageBytes.length);
        return imageBytes;
    }

}
