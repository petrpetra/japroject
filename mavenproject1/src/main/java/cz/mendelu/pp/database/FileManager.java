/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.database;

import cz.mendelu.pp.models.FileRecord;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Petan
 */
public class FileManager {

    private final String kolekce = "files";                                     // Defaultni kolekce
    private final MongoDatabase db = new MongoClient().getDatabase("test");     // Definice databazoveho spojeni

    // Vkládání do kolekce
    public void insert(FileRecord newFileRecord) throws ParseException {

        // z objektu File vytvořím JSON string
        Gson gson = new Gson();
        String newFileJson = gson.toJson(newFileRecord);

        //TODO - ochrana proti vkládání duplicit
        // z JSON vytvořím BSON dokument
        Document newDoc = new Document();
        newDoc = Document.parse(newFileJson);

        System.out.println("-----------------------------------------------");
        System.out.println("--- VLOZENI DO DATABAZE --------");
        System.out.println("-----------------------------");
        System.out.println(newFileJson);
        System.out.println("--- -----------------------------------------");

        // ten vložím do databaze
        db.getCollection(kolekce).insertOne(newDoc);
    }

    public void delete(Integer id) {
        BasicDBObject document = new BasicDBObject();
        db.getCollection(kolekce).deleteOne(new Document("id", id));
    }

    // Zobrazit všechno v kolekci
    // vytup JSON 
    public List<FileRecord> list() throws ParseException {

        FindIterable<Document> iterable = db.getCollection(kolekce).find();
        List<FileRecord> resultList = new ArrayList();
        Gson gson = new Gson();

        List<Document> documents = db.getCollection(kolekce).find().into(new ArrayList<Document>());

        for (Document doc : documents) {
            String jsonString = doc.toJson();
            FileRecord obj = gson.fromJson(jsonString, FileRecord.class);
            resultList.add(obj);
            System.out.println(jsonString);
        }

        return resultList;
    }

    // Vyhledat na základě parametrů
    public List<FileRecord> find(Map<String, String> filters) {
        List<FileRecord> resultList = new ArrayList();
        Gson gson = new Gson();

        // Vytvoření FILTRU ,který se pak dá do hledání
        Document filtr = new Document();

        // podmínky do filtru
        for (Map.Entry<String, String> entry : filters.entrySet()) {
            filtr.append(entry.getKey(), entry.getValue());
        }
        
        //filtr.append("categories", "tapety");

        List<Document> documents = db.getCollection(kolekce).find(filtr).into(new ArrayList<Document>());

        for (Document doc : documents) {
            String jsonString = doc.toJson();
            FileRecord obj = gson.fromJson(jsonString, FileRecord.class);
            resultList.add(obj);
            //System.out.println(jsonString);
        }

        return resultList;
    }

}
