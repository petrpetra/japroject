package cz.mendelu.pp.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 *
 * @author Petan
 *
 * Tento objekt pouze reprezentuje soubor Je to jen takovej kontejner Pomocí
 * GSON se dá z objektu jednoduše udělat JSON string
 */
public class FileRecord{

    // Blok neměnných atributů
    public final Integer id;
    public final String filename;
    public final String username;
    public final String extension;
    public final String size; // naformatovany na MB
    public final Date created_date;
    public final String file_url;

    //Blok měnných atributů
    public Date expiration_date;
    public Integer downloads;
    public List<String> categories = new ArrayList();
    public Boolean permission;
    public String share_key;

    public FileRecord(String filename, String username, String extension, String size, Date created_date, Date expiration_date, String file_url) {
        this.filename = filename;
        this.username = username;
        this.extension = extension;
        this.size = size;
        this.created_date = created_date;
        this.expiration_date = expiration_date;
        this.file_url = file_url;

        id = this.hashCode();
    }


    public Integer getId() {
        return id;
    }
    
    

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }

    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public void setPermission(Boolean permission) {
        this.permission = permission;
    }

    public void setShare_key(String share_key) {
        this.share_key = share_key;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.filename);
        hash = 79 * hash + Objects.hashCode(this.username);
        hash = 79 * hash + Objects.hashCode(this.created_date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileRecord other = (FileRecord) obj;
        if (!Objects.equals(this.filename, other.filename)) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.created_date, other.created_date)) {
            return false;
        }
        return true;
    }

    public String getFilename() {
        return filename;
    }

    public String getUsername() {
        return username;
    }

    public String getExtension() {
        return extension;
    }

    public String getSize() {
        return size;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public String getFile_url() {
        return file_url;
    }

    public Date getExpiration_date() {
        return expiration_date;
    }

    public Integer getDownloads() {
        return downloads;
    }

    public List<String> getCategories() {
        return categories;
    }

    public Boolean getPermission() {
        return permission;
    }

    public String getShare_key() {
        return share_key;
    }

}
