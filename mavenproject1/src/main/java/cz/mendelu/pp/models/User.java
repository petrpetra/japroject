/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.pp.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Petra
 */
@ManagedBean
@SessionScoped

public class User implements Serializable {

    private static final long serialVersionUID = 4256272866128337548L;
    private String id_user;
    private String name;
    private String email;
    private String password;
    private UploadedFile image;
    private String loginType;
    private boolean isSignedIn = false;
    private String verificationAnswer;
    private String picture_url;
    private String verificationQuestion;


    public User() {}

    @Override
    public String toString() {
        return "User{" + "id_user=" + id_user + ", name=" + name + ", email=" + email + ", password=" + password + ", image=" + image + ", loginType=" + loginType + ", isSignedIn=" + isSignedIn + ", verificationAnswer=" + verificationAnswer + ", picture_url=" + picture_url + ", verificationQuestion=" + verificationQuestion + '}';
    }
    
    

    public String getVerificationAnswer() {
        return verificationAnswer;
    }

    public void setVerificationAnswer(String verificationAnswer) {
        this.verificationAnswer = verificationAnswer;
    }
    
    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }
    
    public boolean isIsSignedIn() {
        return isSignedIn;
    }

    public void setIsSignedIn(boolean isSignedIn) {
        this.isSignedIn = isSignedIn;
    }

    public String getVerificationQuestion() {
        return verificationQuestion;
    }

    public void setVerificationQuestion(String verificationQuestion) {
        this.verificationQuestion = verificationQuestion;
    }

    public User(String id_user, String name, String loginType) {
        this.id_user = id_user;
        this.name = name;
        this.loginType = loginType;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public UploadedFile getImage() {
        return image;
    }

    public void setImage(UploadedFile image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.id_user);
        hash = 41 * hash + Objects.hashCode(this.name);
        hash = 41 * hash + Objects.hashCode(this.email);
        hash = 41 * hash + Objects.hashCode(this.password);
        hash = 41 * hash + Objects.hashCode(this.image);
        hash = 41 * hash + Objects.hashCode(this.loginType);
        hash = 41 * hash + (this.isSignedIn ? 1 : 0);
        hash = 41 * hash + Objects.hashCode(this.verificationQuestion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.id_user, other.id_user)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.image, other.image)) {
            return false;
        }
        if (!Objects.equals(this.loginType, other.loginType)) {
            return false;
        }
        if (this.isSignedIn != other.isSignedIn) {
            return false;
        }
        if (!Objects.equals(this.verificationQuestion, other.verificationQuestion)) {
            return false;
        }
        return true;
    }

}
