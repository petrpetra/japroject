<%-- 
    Document   : pomocnej
    Created on : 23.1.2016, 23:03:54
    Author     : Petra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>JSP Page</title>
        </head>
        <body>
            <h1><h:outputText value="Hello World!"/></h1>
        </body>
    </html>
</f:view>
<h:form enctype="multipart/form-data">
    <h:outputText value="#{fileUploadBean.uploadStatus}" />
    <p:fileUpload value="#{fileUploadBean.file}" mode="simple" skinSimple="false" auto="true"/>
    <p:panelMenu>
        <p:submenu label="#{msg.category_file}" style="float:left;width:500px;" expanded="false">
            <p:menuitem >
                <p:selectManyCheckbox id="basic" value="#{fileUploadBean.selectedCategories}" columns="2" layout="grid">
                    <f:selectItems value="#{mainbean.allCategories}" var="category" itemLabel="#{category}" itemValue="#{category}"  />
                </p:selectManyCheckbox>
                <p:inputText value="" placeholder="#{msg.new_category_placeholder}" />
                <br/>
                <p:commandButton value="#{msg.new_category_btt}" ajax="true" actionListener="#{mainbean.addToNewCategories()}" />
            </p:menuitem>
            <p:separator/>
            <p:menuitem>

            </p:menuitem>
        </p:submenu>
        <p:submenu label="#{msg.upload_duration}" style="float:left;" expanded="true">
            <p:menuitem>
                <h:outputLabel for="days" value="#{msg.upload_days}" />
                <p:spinner id="days" value="#{fileUploadBean.uploadDays}" min="5" max="60"/>
            </p:menuitem>
        </p:submenu>

    </p:panelMenu>
</h:form>

<f:selectItem itemLabel="#{msg.question1}" itemValue="#{msg.question1}" />
<f:selectItem itemLabel="#{msg.question2}" itemValue="#{msg.question2}" />
<f:selectItem itemLabel="#{msg.question3}" itemValue="#{msg.question3}" />
<f:selectItem itemLabel="#{msg.question4}" itemValue="#{msg.question4}" />


 <h:panelGrid columns="4" >
                    <p:panel style="background: transparent;">
                        <p:fileUpload value="#{fileUploadBean.file}" mode="simple" skinSimple="true" label="#{msg.choose_file}"  />
                    </p:panel>
                    <p:panel style="border:0px;background: transparent;">

                    </p:panel>
                    <p:panel style="border:0px;background: transparent;padding: 0px;margin:0px;">
                        <p:inputText value="#{mainbean.newCategory}" placeholder="#{msg.new_category_placeholder}" style="min-width: 300px;"/>
                        <br/>
                        <p:commandButton value="#{msg.new_category_btt}" update="category_menu" actionListener="#{mainbean.addToNewCategories()}" style="float:right;margin-top:8px;"/>
                    </p:panel>
                    <p:panel style="border:0px;background: transparent;padding: 0px;margin:0px;">
                        <h:outputLabel for="daysw" value="#{msg.upload_duration} " />
                        <p:spinner id="daysw" value="#{fileUploadBean.uploadDays}" min="5" max="60"/>
                    </p:panel>




                </h:panelGrid>