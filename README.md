# README #

Náš skvělej projekt do předmětu Java Aplikace.
Peťo kdyžtak sem doplň co tě napadne ;)

### 14.12.2015 ###
* PostgreSQL 9.5
* [návod](http://pettan.cz/subdom/f/jak_na_db.pdf)


### 13.12.2015 ###
* Přihlašování přes ScribeJava (jendnoduší, zatim fb ,ale je to připravený na další stránky)
* MainBean + index.xhtml = uživatelova stránka


### 8.12.2015 ###
* Konečně facebook přihlašování.
* Na mongoDB se pracuje.
* Na IBM bluemix se SERE.

### 2.12.2015 ###
* [Návrh struktury souboru v mongoDB](https://docs.google.com/drawings/d/1-OQEflgym316Lj39ohUeB248VxXTvzsHGkBO-EmND5Y/edit?usp=sharing)




### Hotlinks ###

* [Japroject Facebook App](https://developers.facebook.com/apps/447567858764225/dashboard/)
* [Google Dokument](https://docs.google.com/document/d/1-Jd8OYDhNh3ZfdxnItzzA2s1eX42mqEUhXVKYlhUrBw/edit)
* [BlueMix](https://console.ng.bluemix.net/) - tady to asi nahodíme
* [Front-end framework](http://primefaces.org/) - vypadá to luxusně :D

### Co by to mělo mít / umět? ###

* Přihlašování bez registrace ,pomocí Facebooku (případně dalších služeb).
* Možnost uploadovat soubory, mazat je. 
* Měnit práva / nastavení / nějaké další atributy nahraných souborů.
* Vygenerovat odkazy na soubory pro sdílení (permanentní, dočasné na určitou dobu).
* Nějaké moc hezké filtrování souborů (třeba podle datumu, přípon, viz [ta moje blbina](http://pettan.cz/subdom/root/index.php))
* Nakonec krásnej design. 

### Co se pod tím bude skrývat (back-end) ? ###

* Java EE Maven aplikace
* [MongoDB - noSQL databáze](https://www.mongodb.org/)
* asi [OAuth přihlašování](http://oauth.net/)
* ...

### Co je lepší? ###

* Psi
* Kočky